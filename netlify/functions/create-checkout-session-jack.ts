import Big from "big.js";

const stripePromise = import("stripe").then((stripeModule) => {
  const Stripe = stripeModule.default as typeof import("stripe").default;
  return new Stripe(process.env.STRIPE_SECRET_KEY as string, {
    apiVersion: "2024-04-10",
  });
});

exports.handler = async (event: any, context: any, callback: any) => {
  const { items, email, quote_id, contactNumber, distribution } = JSON.parse(event.body);
  const stripe = await stripePromise;

  // Convert grouped items to an array of line items
  const transformedItems = Object.values(items).map((item: any) => {
    let product_data: { name: string; images: string[]; description?: string } = {
      name: item.title,
      images: [item.image],
      description: item.description,
    };

    return {
      price_data: {
        currency: "gbp",
        unit_amount: Number(Big(item.price).times(100).toFixed(0)),
        product_data,
      },
      quantity: item.quantity,
    };
  });

  try {
    console.log("Elliot's labour distribution:", distribution.labour.elliot);
    console.log("Elliot's material distribution:", distribution.material.elliot);
    console.log("Stringified distribution:", JSON.stringify(distribution));
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      line_items: transformedItems,
      mode: "payment",
      phone_number_collection: {
        enabled: true,
      },
      success_url: `${process.env.HOST}/success`,
      cancel_url: `${process.env.HOST}/costs/${quote_id}`,
      metadata: {
        email,
        quote_id,
        contactNumber,
        items: JSON.stringify(items),
        distribution: JSON.stringify(distribution),
        session_type: "elliot",
      },
    });

    callback(null, {
      statusCode: 200,
      body: JSON.stringify({ id: session.id }),
    });
  } catch (error) {
    console.error("Error creating checkout session:", error);

    callback(null, {
      statusCode: 500,
      body: JSON.stringify({ error }),
    });
  }
};