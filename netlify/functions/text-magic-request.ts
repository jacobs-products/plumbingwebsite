import axios from 'axios';

exports.handler = async (event: any, context: any) => {
  if (event.httpMethod !== 'POST') {
    return { statusCode: 405, body: 'Method Not Allowed' };
  }

  const { text, phones } = JSON.parse(event.body);

  try {
    const response = await axios.post('https://rest.textmagic.com/api/v2/messages', {
      text,
      phones,
    }, {
      headers: {
        'X-TM-Username': 'info@theplumber.co.uk',
        'X-TM-Key': process.env.TEXTMAGIC_API_KEY,
      },
    });

    return { statusCode: 200, body: JSON.stringify(response.data) };
  } catch (error: any) {
    return { statusCode: 500, body: error.toString() };
  }
};