import axios from 'axios';

exports.handler = async (event: any, context: any) => {
  if (event.httpMethod !== 'POST') {
    return { statusCode: 405, body: 'Method Not Allowed' };
  }

  const { text, phones } = JSON.parse(event.body);

  const twentyFourHoursLater = Math.floor(Date.now() / 1000) + 86400;

  try {
    const response = await axios.post('https://rest.textmagic.com/api/v2/messages', {
      text,
      phones,
      sendingTime: twentyFourHoursLater,
    }, {
      headers: {
        'X-TM-Username': 'info@theplumber.ltd',
        'X-TM-Key': process.env.TEXTMAGIC_API_KEY,
      },
    });

    return { statusCode: 200, body: JSON.stringify(response.data) };
  } catch (error: any) {
    return { statusCode: 500, body: error.toString() };
  }
};