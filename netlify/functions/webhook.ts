import Big from "big.js";
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

async function checkAccountCapabilities(stripe: any, accountId: any) {
  const account = await stripe.accounts.retrieve(accountId);
  if (account.capabilities.transfers !== 'active') {
    throw new Error(`Account ${accountId} does not have transfer capabilities enabled.`);
  }
}

exports.handler = async (event: any, context: any, callback: any) => {
  console.log("Webhook received");

  const sig = event.headers['stripe-signature'];
  let session;
  
  const payload = event.body ? (event.isBase64Encoded ? Buffer.from(event.body, 'base64').toString('utf8') : event.body) : null;

  if (!payload) {
    return callback(null, {
      statusCode: 400,
      body: "Missing request body",
    });
  }

  if (process.env.STRIPE_SIGNING_SECRET === undefined) {
    throw new Error("Stripe signing secret is not defined in the environment variables.");
  }

  try {
    session = stripe.webhooks.constructEvent(payload, sig, process.env.STRIPE_SIGNING_SECRET);
  } catch (err: any) {
    console.error('Webhook signature verification failed.', err.message);
    return callback(null, {
      statusCode: 200, // Always return 200 to prevent retries
      body: `Webhook Error: ${err.message}`,
    });
  }

  if (session.type === 'checkout.session.completed') {
    const metadata = session.data.object.metadata;

    console.log("metadata:", metadata);
    console.log("sessiontype:", metadata.session_type);
    console.log("distribution:", metadata.distribution);
    // Validate metadata
    if (!metadata || !metadata.session_type || !metadata.distribution) {
      console.error("Invalid or missing metadata for session");
      return callback(null, {
        statusCode: 200, // Always return 200 to prevent retries
        body: "Invalid or missing metadata for session",
      });
    }

    // Only proceed if session_type is "elliot"
    if (metadata.session_type === "elliot") {
      const { quote_id, distribution: distributionString, items } = metadata;
      const elliotAccountId = process.env.ELLIOT_ACCOUNT_ID;
      const jackAccountId = process.env.JACK_ACCOUNT_ID;

      if (!elliotAccountId || !jackAccountId) {
        console.error("Account IDs are not defined in the environment variables.");
        return callback(null, {
          statusCode: 200, // Always return 200 to prevent retries
          body: "Account IDs are not defined in the environment variables.",
        });
      }

      // Parse distribution from string to object
      const distribution = JSON.parse(distributionString);

      // Check capabilities for Elliot's account
      await checkAccountCapabilities(stripe, elliotAccountId);

      const transfers = [];
      for (const item of JSON.parse(items)) {
        const labourAmountElliot = Number(Big(item.price).times(distribution.labour.elliot).times(100).toFixed(0));
        const materialAmountElliot = Number(Big(item.price).times(distribution.material.elliot).times(100).toFixed(0));

        // Transfer for Elliot's share of labour
        transfers.push(
          stripe.transfers.create({
            amount: labourAmountElliot,
            currency: "gbp",
            destination: elliotAccountId,
            transfer_group: `ORDER_${quote_id}`,
          })
        );

        // Transfer for Elliot's share of materials
        transfers.push(
          stripe.transfers.create({
            amount: materialAmountElliot,
            currency: "gbp",
            destination: elliotAccountId,
            transfer_group: `ORDER_${quote_id}`,
          })
        );

        // No transfer for Jack's share (remains in your account)
      }

      try {
        await Promise.all(transfers);
      } catch (err: any) {
        console.error("Error processing transfers:", err.message);
        return callback(null, {
          statusCode: 200, // Always return 200 to prevent retries
          body: `Error processing transfers: ${err.message}`,
        });
      }
    }
  }

  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ received: true }),
  });
};