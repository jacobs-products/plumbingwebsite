import type { APIGatewayProxyEvent, Context, Callback } from 'aws-lambda';
import axios from 'axios';

const adminPromise = import("firebase-admin").then((adminModule) => {
  const admin = adminModule.default;
  return { admin };
});

const stripePromise = import("stripe").then((stripeModule) => {
  const Stripe = stripeModule.default;
  if (typeof process.env.STRIPE_SECRET_KEY === "string") {
    return new Stripe(process.env.STRIPE_SECRET_KEY, {
      apiVersion: "2024-04-10",
    });
  } else {
    throw new Error("STRIPE_SECRET_KEY environment variable is not defined");
  }
});

// Secure a connection to Firebase from backend
const serviceAccount = {
  projectId: process.env.FIREBASE_ADMIN_PROJECT_ID,
  privateKey: process.env.FIREBASE_ADMIN_PRIVATE_KEY
    ? process.env.FIREBASE_ADMIN_PRIVATE_KEY.replace(/\\n/gm, "\n")
    : undefined,
  clientEmail: process.env.FIREBASE_ADMIN_CLIENT_EMAIL,
};

const appPromise = adminPromise.then(({ admin }) => {
  const appName = 'PlumbingWebsite';
  if (!admin.apps.find(app => (app as any).name === appName)) {
    return admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
    }, appName);
  } else {
    return admin.app(appName);
  }
});

const endpointSecret = process.env.STRIPE_SIGNING_SECRET;
if (typeof endpointSecret !== 'string') {
  throw new Error('Missing or invalid STRIPE_ENDPOINT_SECRET environment variable');
}

const fulfillOrder = async (session: any) => {
  const { admin } = await adminPromise;
  const app = await appPromise;

  let paymentId: string | undefined;
  let amount: number | undefined;
  let amount_shipping: number | undefined;
  let images: string[] | undefined;
  let title: string[] | undefined;
  let email: string | undefined;
  let quote_id: string | undefined;
  let contactNumber: string | undefined;

  const stripe = await stripePromise;
  if (session.payment_method_types && session.payment_method_types.includes("card")) {
    // Get the id of the Stripe Payment object
    const paymentIntent = await stripe.paymentIntents.retrieve(session.payment_intent);
    const charges = await stripe.charges.list({ payment_intent: paymentIntent.id });

    if (charges.data.length > 0 && charges.data[0].payment_method) {
      paymentId = charges.data[0].payment_method as string;
    }
    amount = session.amount_total / 100;
    amount_shipping = session.total_details.amount_shipping / 100;
    images = session.metadata.images;
    title = JSON.parse(session.metadata.title);
    email = session.metadata.email;
    quote_id = session.metadata.quote_id;
    contactNumber = session.metadata.contactNumber;
    
    await app
      .firestore()
      .collection("users")
      .doc(email as string)
      .collection("orders")
      .doc(session.id)
      .set({
        amount,
        title,
        timestamp: admin.firestore.FieldValue.serverTimestamp(),
        id: paymentId,
        quote_id: quote_id,
      });

    console.log(`SUCCESS: Order ${session.id} has been added to the DB`);

    const quoteDetails = `https://trade-safe.netlify.app/invoices/${quote_id}`;
    
    // Call your Netlify function to send the quote to the customer
    await axios.post((process.env.HOST + '/.netlify/functions/text-magic-request'), {
        text: quoteDetails,
        phones: "+44" + contactNumber,
    });
    console.log("text magic request sent");
  }
};

exports.handler = async (event: APIGatewayProxyEvent, context: Context, callback: Callback) => {
  if (event.httpMethod === "POST") {
    const payload = event.body;
    let sig = event.headers["stripe-signature"];
    let checkoutEvent;

    // Verify that the Event posted came from Stripe
    if (sig) {
      try {
        if (payload === null) {
          throw new Error('Missing request body');
        }
        const stripe = await stripePromise;
        checkoutEvent = stripe.webhooks.constructEvent(payload, sig, endpointSecret);
      } catch (err) {
        if (err instanceof Error) {
          console.log("ERROR", err.message);
        } else {
          console.log("ERROR", err);
        }
        return callback(null, {
          statusCode: 400,
          body: `Webhook error: ${err instanceof Error ? err.message : 'Unknown error'}`,
        });
      }
    }

    // Handle the checkout.session.completed event
    if (
      checkoutEvent &&
      checkoutEvent.type === "checkout.session.completed"
    ) {
      const session = checkoutEvent.data.object;

      // Fullfill order
      return fulfillOrder(session)
        .then(() => callback(null, { statusCode: 200 }))
        .catch((err) => {
          callback(null, {
            statusCode: 400,
            body: `Webhook Error: ${err instanceof Error ? err.message : 'Unknown error'}`,
          });
        });
    }
  }
};