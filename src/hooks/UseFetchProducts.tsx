import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import db from '../../firebase';
import {
  collection,
  addDoc,
  serverTimestamp,
  query,
  where,
  getDocs,
  onSnapshot,
} from 'firebase/firestore';
import {
  ProductState,
  setProducts,
  setLoading,
  setError,
  setLastUpdated,
} from '../slices/ProductSlice';
import { IProduct } from '../../typings';

export const useFetchProducts = () => {
  // const dispatch = useDispatch();

  // type RootState = {
  //   product: ProductState;
  // };

  // useEffect(() => {
  //   const fetchProducts = async () => {
  //     try {
  //       const productsRef = collection(db, 'products');
  //       onSnapshot(productsRef, (snapshot) => {
  //         const updatedProducts: IProduct[] = [];
  //         snapshot.forEach((doc) => {
  //           updatedProducts.push(doc.data() as IProduct);
  //         });

  //         dispatch(setProducts(updatedProducts));
  //         dispatch(setLoading(false));
  //       });
  //     } catch (err) {
  //       dispatch(setError('Error fetching products.'));
  //       dispatch(setLoading(false));
  //     }
  //   };

  //   fetchProducts();
  // }, []);
};