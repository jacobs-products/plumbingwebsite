import React, { useState } from 'react';

const Footer = () => {
  const [hover, setHover] = useState(false);
  const toggleHover = () => setHover(!hover);

  return (
    <div style={{
      paddingTop: '0px',
      paddingBottom: '0px',
      position: 'fixed',
      bottom: '0',
      width: '100%',
      borderTop: '2px solid #209ee6',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: window.innerWidth <= 768 ? '7em' : '11em',
      backgroundColor: 'white'
    }}>
      <h1 style={{
        fontSize: window.innerWidth <= 768 ? '2.5em' : '3.8em',
        fontWeight: '600',
        color: hover ? '#444444' : '#209ee6',
        fontFamily: 'Montserrat, sans-serif',
        letterSpacing: '-0.05em'
      }}>
        <a href="Tel:01322771767" onMouseEnter={toggleHover} onMouseLeave={toggleHover}>01322 771767</a>
      </h1>
    </div>
  );
};

export default Footer;