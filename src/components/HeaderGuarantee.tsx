import {
  Bars3Icon,
  MagnifyingGlassIcon,
  ShoppingCartIcon,
} from "@heroicons/react/24/outline";
import { signIn, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { selectItems } from "../slices/basketSlice";
import { useEffect, useState } from "react";
import { useCategories } from '../hooks/useCategories';

type Props = {};

const HeaderGuarantee = (props: Props) => {
  const [windowWidth, setWindowWidth] = useState(0);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const { data: session, status } = useSession();
  const [openCategoryIndex, setOpenCategoryIndex] = useState<number | null>(null);
  const [openSubcategoryIndex, setOpenSubcategoryIndex] = useState<number | null>(null);
  const router = useRouter();
  const items = useSelector(selectItems);

  // Use the categories custom hook to get the list of categories
  const categories = useCategories();
  
  useEffect(() => {
    // Update window width on mount and resize
    setWindowWidth(window.innerWidth);
    const handleResize = () => setWindowWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);

    // Cleanup event listener on unmount
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const handleSearch = () => {
    router.push(`/search?q=${searchQuery}`);
  };

  const smallScreenStyles = windowWidth < 768 ? { transform: 'scale(1.2)', marginTop: '-14px', marginBottom: '-84px' } : {};

  return (
    <header style={{ ...smallScreenStyles, position: 'relative', zIndex: 100 }}>
      {/* top nav */}
      <div className="flex items-center p-1 flex-grow py-2 bg-white">
      </div>
      <div className="container mx-auto flex items-center justify-start max-w-full flex-grow" style={{ maxWidth: '1261px', maxHeight: '262px' }}>
        <div style={{ backgroundColor: 'white', overflow: 'hidden' }}>
          <Image
            className="cursor-pointer mt-2"
            src="/trade-safe-guarantee.png"
            width={1080}
            height={250}
            alt="LinguaPro"
            style={{ outline: 'none', boxShadow: 'none' }}
          />
        </div>
      </div>
      <div className="">
      </div>
    </header>
  );
};

export default HeaderGuarantee;