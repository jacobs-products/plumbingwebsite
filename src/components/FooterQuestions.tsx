import React from 'react';

const FooterQuestions = () => {
  return (
    <div style={{
      paddingTop: '0px',
      paddingBottom: '0px',
      position: 'fixed',
      bottom: '0',
      width: '100%',
      borderTop: '2px solid #209ee6',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: window.innerWidth <= 768 ? '7em' : '11em',
      backgroundColor: 'white'
    }}>
      <strong>Any Questions?</strong>
      <a href="SMS:07451275575" style={{ 
          background: "none", 
          border: "none", 
          padding: 0,
        }}
        onClick={(e) => {
          if (!navigator.userAgent.match(/Android|iPhone|iPad|iPod/i)) {
          e.preventDefault();
          alert("Please send a text wiht your questions to the following number: 07451275575");
          }
      }}>
        <img src="/questionsbutton.png" alt="Questions" style={{ 
          width: window.innerWidth <= 768 ? '150px' : '200px',
          height: 'auto'
        }}/>
      </a>
    </div>
  );
};

export default FooterQuestions;

