import moment from "moment";
import numeral from "numeral";
import { IOrder } from "../../typings";
import Link from 'next/link';

type Props = {
  order: IOrder;
};

const Order = ({ order }: Props) => {
  return (
    <div className="relative border rounded-md">
      <div className="flex items-center space-x-10 p-5 bg-gray-100 text-sm text-gray-600">
        <div>
          <p className="font-bold text-xs">ORDER PLACED</p>
          <p className="font-bold text-xs">
            {moment.unix(order.timestamp).format("DD MMM YYYY")}
          </p>
        </div>

        <div>
          <p className="text-xs font-bold">TOTAL</p>
          <p>
            £{numeral(order.amount).format('£0,0.00')}
          </p>
        </div>
        <p className="absolute top-2 right-2 w-40 lg:w-72 truncate text-xs whitespace-nowrap">
          ORDER ID: {order.id}
        </p>
      </div>

      <div className="p-5 sm:p-10">
        <div className="flex space-x-6 overflow-x-auto">
          <p>
            Invoice:
          </p>
          <Link href={`/invoices/${order.quote_id}`}>
            <a className="text-blue-500 hover:text-purple-500 hover:underline">View Invoice</a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Order;