import { useState, useEffect } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderConnor from '../components/HeaderConnor';
import { addDoc, collection, doc, getDocs, limit, orderBy, query, runTransaction, serverTimestamp, setDoc, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const QuoteForm2 = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '07944731036',
  });

  const [details, setDetails] = useState<Array<{ type: string, description: string, quantity: string, cost: string }>>([]); 

  const [successMessage, setSuccessMessage] = useState('');

  const [link, setLink] = useState("https://trade-safe.co.uk/a50/");
  const [totalCost, setTotalCost] = useState(49);
  const [productCategory, setProductCategory] = useState('');
  const [selectedPlumber, setSelectedPlumber] = useState('Connor the Plumber');
  const [fixedQuote, setFixedQuote] = useState(''); // 1.3

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      [e.target.name]: e.target.value,
    });
  };

  // Handle input change for labour details
  const handleCostChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if(e.target.value)
    {
      setTotalCost(parseInt(e.target.value));
      const selectElement = document.querySelector('select[name="cost"]');
      if (selectElement) {
        const selectedIndex = (selectElement as HTMLSelectElement).selectedIndex;
        const options = (selectElement as HTMLSelectElement).options;
        let foundFirstIteration = false;
        for (let i = 0; i < options.length; i++) {
          if (options[i].value === e.target.value) {
            if (foundFirstIteration && i === selectedIndex) {
              setProductCategory('secondversion');
              break;
            }
            if (!foundFirstIteration) {
              foundFirstIteration = true;
            }
          }
        }
      }
    }
  };

  const handleDetailsChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const newDetails = [...details];
    const key = e.target.name as keyof typeof newDetails[0];
    newDetails[index][key] = e.target.value;
    setDetails(newDetails);
  };

  // Add a new labour detail section
  const addLabourDetailSection = () => {
    setDetails([...details, {
      type: 'Labour',
      description: '',
      quantity: '',
      cost: '',
    }]);
  };

  // Handle delete operation for labour details
  const handleDelete = (index: number) => {
    const newDetails = [...details];
    newDetails.splice(index, 1);
    setDetails(newDetails);
  }

  // Handle plumber selection
  const handlePlumberChange = (e: any) => {
    setSelectedPlumber(e.target.value);
    if (e.target.value === 'Connor the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07944731036',
      });
    } else if (e.target.value === 'Jack the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07483806453',
      });
    } else {
      setClientDetails({
        ...clientDetails,
        contactNumber: '',
      });
    }
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    // Check if all required fields are filled
    if (!fixedQuote) {
      alert('Please fill in all the required fields.');
      return;
    }

    
    let newId;
    try {
      // Reference to the document that holds the latest ID
      const metadataRef = doc(db, 'metadata', 'latestProductId');
  
      // Use a transaction to ensure the operation is atomic
      await runTransaction(db, async (transaction) => {
        const metadataSnap = await transaction.get(metadataRef);
        let maxId = 0;
  
        if (metadataSnap.exists()) {
          maxId = metadataSnap.data().id;
        } else {
          // Create the metadata document if it doesn't exist
          await setDoc(metadataRef, { id: maxId });
        }
  
        // Generate a new ID based on the latest ID
        newId = (maxId + 1).toString();
  
        // Update the latest ID in the metadata document
        transaction.update(metadataRef, { id: parseInt(newId) });
  
        // Prepare the new product data
        const newProduct = {
          id: newId,
          title: clientDetails.clientName,
          price: totalCost,
          description: '',
          category: productCategory,
          image: '',
          quantity: 1,
          clientDetails,
          details,
          fixedQuote,
        };
  
        // Add the new product document
        await addDoc(collection(db, 'products'), newProduct);
      });
  
      // Update the lastUpdated collection (if needed)
      const lastUpdatedRef = collection(db, 'lastUpdated');
      const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
      const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
      const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
      if (lastUpdatedDocRef) {
        await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
      } else {
        console.error('Last updated document not found');
      }

    } catch (error) {
        console.error("An error occurred:", error);
    } finally {
        try {
            const quoteDetails = `https://trade-safe.netlify.app/initialquotes2/${newId}`;

            await axios.post('/.netlify/functions/text-magic-request', {
                text: quoteDetails,
                phones: "+44" + clientDetails.contactNumber,
            });
        } catch (error) {
            console.error("An error occurred while sending the text:", error);
        }
        setSuccessMessage('Quote sent successfully!');
    }
  }

  // Handle input change for description
  const handleDescriptionChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDetails([{ type: 'Labour', description: e.target.value, quantity:"1", cost:"49" }]);
  };

  
  // 1.3 - Handle fixed quote change
  const handleFixedQuoteChange = (e: any) => {
    setFixedQuote(e.target.value);
  };

return (
  <div style={{ justifyContent:'center', alignItems:'center', fontFamily:'Arial,sans-serif', width:'75%', margin:'0 auto' }}>
    <HeaderConnor /> {/* Include the header */}
    <div style={{ display:'flex', flexDirection:'column', alignItems:'center', padding:'5px', width:'100%', border:'2px solid #209ee6', marginTop:'100px' }}>
      <h1 style={{ color:'#000000', textAlign:'center', fontSize:'40px', fontWeight:'bold' }}><span style={{ borderBottom:'4px solid #209ee6' }}>Costs</span></h1>
      <form onSubmit={handleSubmit} style={{ width:'100%' }}>
        {/* Plumber Selection */}
        <select name="plumber" onChange={handlePlumberChange} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}>
        <option value="Connor the Plumber">Connor the Plumber</option>
          <option value="Jack the Plumber">Jack the Plumber</option>
        </select>
        {/* Labour and Material Details Section */}
        <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
          <br />
          {/* 1.3 - Fixed Quote Section */}
          <div>
            <p>Fixed Quote*:</p>
            <div>
              <input type="radio" id="yes" name="fixedQuote" value="1" onChange={handleFixedQuoteChange} required/>
              <label htmlFor="yes">Yes</label>
              {/* <button style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => { e.preventDefault(); handleFixedQuoteChange('You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster'); }}>i</button> */}
              <br />
              <input type="radio" id="no" name="fixedQuote" value="2" onChange={handleFixedQuoteChange} required/>
              <label htmlFor="no">No</label>
              {/* <button style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => { e.preventDefault(); handleFixedQuoteChange('We’ll do our best to complete the job within this quote, but we may need more time which will be quoted for separately (if needed) with your approval before we proceed'); }}>i</button> */}
              {fixedQuote == "1" ? 
                <p>You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster</p> 
                  : fixedQuote == "2" ? 
                      <p>We’ll do our best to complete the job within this quote, but we may need more time which will be quoted for separately (if needed) with your approval before we proceed</p> 
                  : <p></p>
              }
            </div>
          </div>
          <br />
          <textarea name="quote" onChange={handleDescriptionChange} placeholder={`Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} />
          <div>
            <br />
            <p>Labour Cost (£):</p>
            <select name="cost" onChange={handleCostChange} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}>
              <option value="49" selected>Half Hour - £49</option>
              <option value="98">1 Hour - £98</option>
              <option value="147">1.5 hours - £147</option>
              <option value="196">2 Hours - £196</option>
              <option value="245">2.5 hours - £245</option>
              <option value="294">3 Hours - £294</option>
              <option value="343">3.5 Hours - £343</option>
              <option value="392">4 Hours - £392</option>
              <option value="490">5 Hours - £490</option>
              <option value="588">Full Day - £588</option>
            </select>
          </div>
        </div>

        {/* Display totalCost and Total */}
        <div style={{ marginTop:'10px' }}>
          <p>Labour Total:</p>
          <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{totalCost.toFixed(2)}</div>
        </div>
        <br />
        {/* Submit Button */}
        <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
          <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
          <img src="/orangesendbutton.png" alt="approve" />
          </button>
        </div>
        <p>{successMessage}</p>
      </form>
    </div>
  </div>
  );  
};

export default QuoteForm2;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
  ) => {
      const session = await getSession(context).catch((error) => {
        console.error('Error getting session:', error);
        return null;
      });

      return {
      props: {
      session,
    },
  };
};