import { useState } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import Header from '../components/Header';
import { addDoc, collection, getDocs, query, serverTimestamp, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const Home = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '',
  });

  const [details, setDetails] = useState<Array<{ type: string, description: string, quantity: string, cost: string }>>([]); 

  const [successMessage, setSuccessMessage] = useState('');

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      [e.target.name]: e.target.value,
    });
  };

  // Handle input change for labour and material details
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const newDetails = [...details];
    const key = e.target.name as keyof typeof newDetails[0];
    newDetails[index][key] = e.target.value;
    setDetails(newDetails);
  };

  // Add a new labour detail section
  const addLabourDetailSection = () => {
    setDetails([...details, {
      type: 'Labour',
      description: '',
      quantity: '',
      cost: '',
    }]);
  };

  // Add a new material detail section
  const addMaterialDetailSection = () => {
    setDetails([...details, {
      type: 'Material',
      description: '',
      quantity: '',
      cost: '',
    }]);
  };

  // Handle delete operation for labour and material details
  const handleDelete = (index: number) => {
    const newDetails = [...details];
    newDetails.splice(index, 1);
    setDetails(newDetails);
  };

  // Calculate total cost
  const calculateCosts = () => {
    let totalCost = details.reduce((total, detail) => total + Number(detail.cost) * Number(detail.quantity), 0);
    
    return { totalCost };
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    alert('Please don\'t use this form. It is something that the developer occasionally enables for testing purposes.');
    e.preventDefault();
    // let newId;
    // try {
    //   const querySnapshot = await getDocs(collection(db, 'products'));
    //   let maxId = 0;
    //   querySnapshot.forEach(doc => {
    //     const product = doc.data() as IProduct;
    //     const id = parseInt(product.id);
    //     if (id > maxId) {
    //       maxId = id;
    //     }
    //   });
    //   newId = (maxId + 1).toString();

    //   const newProduct = {
    //     id: newId,
    //     title: clientDetails.clientName,
    //     price: calculateCosts().totalCost,
    //     description: '',
    //     category: '',
    //     image: '',
    //     quantity: 1,
    //     clientDetails,
    //     details,
    //   };

    //   await addDoc(collection(db, 'products'), newProduct);

    //   const lastUpdatedRef = collection(db, 'lastUpdated');
    //   const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
    //   const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
    //   const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
    //   if (lastUpdatedDocRef) {
    //     await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
    //   } else {
    //     console.error('Last updated document not found');
    //   }

    // } catch (error) {
    //     console.error("An error occurred:", error);
    // } finally {
    //     try {
    //         const quoteDetails = `https://trade-safe.netlify.app/initialquotes/${newId}`;

    //         await axios.post('/.netlify/functions/text-magic-request', {
    //             text: quoteDetails,
    //             phones: "+44" + clientDetails.contactNumber,
    //         });
    //     } catch (error) {
    //         console.error("An error occurred while sending the text:", error);
    //     }

    //     setClientDetails({
    //         clientName: '',
    //         street: '',
    //         city: '',
    //         county: '',
    //         postcode: '',
    //         contactNumber: '',
    //     });

    //     setDetails([{
    //         type:'Labour',
    //         description:'',
    //         quantity:'',
    //         cost:'',
    //      },{
    //         type:'Material',
    //         description:'',
    //         quantity:'',
    //         cost:'',
    //      }]);

    //      setSuccessMessage('Quote sent successfully!');
    // }
  }

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <Header /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Quote</span></h1>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>
          {/* Customer Mobile Section */}
          <input type="tel" name="contactNumber" value={clientDetails.contactNumber} onChange={handleClientInputChange} placeholder="Customer Mobile" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
  
          {/* Buttons to add labour and material details */}
          <button type="button" onClick={addLabourDetailSection} style={{ marginTop:'10px', backgroundColor:'#D3D3D3', borderRadius: '5px', paddingLeft: '3px', paddingRight: '3px' }}>Labour <strong>+</strong></button>
          <br />
          <button type="button" onClick={addMaterialDetailSection} style={{ marginTop:'10px', backgroundColor:'#D3D3D3', borderRadius: '5px' }}>Material <strong>+</strong></button>

          {/* Labour and Material Details Section */}
          {details.map((detail, index) => (
            <div key={index} style={{ position: 'relative', borderBottom: index !== details.length - 1 ? "1px solid #D3D3D3" : "none", paddingBottom: "10px" }}>
              <button type="button" onClick={() => handleDelete(index)} style={{ position: 'absolute', right: 5, fontWeight: 'bold', borderRadius: '5px' }}>X</button>
              <br />
              <input type="text" name="description" value={detail.description} onChange={(e) => handleInputChange(e, index)} placeholder={`${detail.type} Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
              <div style={{ display:'flex', justifyContent:'space-between' }}>
                <input type="number" name="quantity" value={detail.quantity} onChange={(e) => handleInputChange(e, index)} placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
                <input type="number" name="cost" value={detail.cost} onChange={(e) => handleInputChange(e, index)} placeholder={`Unit Price (£)`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
              </div>
            </div>
          ))}
  
          {/* Display totalCost and Total */}
          <div style={{ marginTop:'10px' }}>
            <p>Total:</p>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCost.toFixed(2)}</div>
          </div>
          <br />
          {/* Submit Button */}
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/submitbutton.png" alt="Submit" />
            </button>
          </div>
          <p>{successMessage}</p>
        </form>
      </div>
    </div>
  );  
};

export default Home;

export const getServerSideProps = async (
 context: GetServerSidePropsContext
) => {
 const session = await getSession(context).catch((error) => {
   console.error('Error getting session:', error);
   return null;
 });

 return {
   props: {
     session,
   },
 };
};