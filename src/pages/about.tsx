import Head from 'next/head';
import Header from '../components/Header';
import Link from 'next/link';

type Props = {};

const About = (props: Props) => {
  return (
    <>
      <Head>
        <title>About | Trade-Safe</title>
      </Head>
      <Header />
      <main className="max-w-screen-2xl mx-auto p-10">
        <h1 className="text-3xl font-bold mb-6">About Trade-Safe</h1>
        <p className="text-lg mb-4">
          Trade-Safe is a business that provides a variety of plumbing services. We pride ourselves on the quality and reliability of our services!
        </p>
        {/* ... */}
      </main>
    </>
  );
};

export default About;