import { useState, useEffect } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderGuarantee from '../components/HeaderGuarantee';

const Message = () => {
  const [clientDetails, setClientDetails] = useState({
    contactNumber: '',
    engineerNumber: '',
    message: '',
  });

  const [successMessage, setSuccessMessage] = useState('');
  const [selectedEngineer, setSelectedEngineer] = useState('');

  const engineerPhoneNumbers: any = {
    "Jack": "07483806453",
    "Elliot": "07949104752",
    "Connor": "07944731036",
    "Ben": "07763439896",
    "Ben 2": "07414279228",
  };

  const handleEngineerChange = (e: any) => {
    setSelectedEngineer(e.target.value);
    setClientDetails({
      ...clientDetails,
      engineerNumber: engineerPhoneNumbers[e.target.value],
    });
  };

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      [e.target.name]: e.target.value,
    });
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    // Check if all required fields are filled
    if (!clientDetails.contactNumber || !clientDetails.message) {
      alert('Please fill in all the required fields.');
      return;
    }

    try {
      let engineer;

      if(selectedEngineer == "Ben 2")
      {
        engineer = "Ben";
      }
      else
      {
        engineer = selectedEngineer;
      }

      const messageDetails = `Hi,\n\n${engineer} the Plumber said "${clientDetails.message}"\n\nJack\nThe Plumber`;

      await axios.post('/.netlify/functions/text-magic-request', {
        text: messageDetails,
        phones: "+44" + clientDetails.engineerNumber + "," + "+44" + clientDetails.contactNumber,
      });

      setSuccessMessage('Message sent successfully!');
    } catch (error) {
      console.error("An error occurred while sending the text:", error);
    }
  };

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <HeaderGuarantee /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}>
          <span style={{ borderBottom: '4px solid #209ee6' }}>Message</span>
        </h1>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>
          {/* Customer Mobile Section */}
          <input
            type="tel"
            name="contactNumber"
            value={clientDetails.contactNumber}
            onChange={handleClientInputChange}
            placeholder="Customer Mobile"
            style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}
            required
          />
          <br />
          <br />
          <div>
            <strong>Engineer:</strong>
            <select value={selectedEngineer} onChange={handleEngineerChange} required>
              <option value="" disabled>Select Engineer</option>
              <option value="Jack">Jack - 07483806453</option>
              <option value="Elliot">Elliot - 07949104752</option>
              <option value="Connor">Connor - 07944731036</option>
              <option value="Ben">Ben - 07763439896</option>
              <option value="Ben 2">Ben 2 - 07414279228</option>
            </select>
          </div>
          {/* Message Box */}
          <textarea
            name="message"
            value={clientDetails.message}
            onChange={handleClientInputChange}
            placeholder="Message"
            rows={4}
            style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}
            required
          ></textarea>
          {/* Submit Button */}
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/orangesendbutton.png" alt="send" />
            </button>
          </div>
          <p>{successMessage}</p>
        </form>
      </div>
    </div>
  );
};

export default Message;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};