import { useState } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderPlumber from '../components/HeaderPlumber';
import { addDoc, collection, doc, getDocs, limit, orderBy, query, runTransaction, serverTimestamp, setDoc, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const ReceiptForm = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '',
    engineerNumber: '',
  });

  const [details, setDetails] = useState({
    labour: { type: 'Labour', description: '', quantity: '1', cost: '' },
    material: { type: 'Material', description: '', quantity: '1', cost: '' }
  });

  const [successMessage, setSuccessMessage] = useState('');
  const [reviewMessage, setReviewMessage] = useState('');
  const [totalCost, setTotalCost] = useState(49);
  const [productCategory, setProductCategory] = useState('');
  const [selectedPlumber, setSelectedPlumber] = useState('Elliot the Plumber');
  const [selectedEngineer, setSelectedEngineer] = useState('');
  const [unableToGuarantee, setUnableToGuarantee] = useState('');
  const [reason, setReason] = useState('');
  const [infoMessage, setInfoMessage] = useState('');
  const [customerFound, setCustomerFound] = useState('');
  const [reviewScheduled, setReviewScheduled] = useState(false);
  const [reviewOptionSelected, setReviewOptionSelected] = useState(false);

  const engineerPhoneNumbers: any = {
    "Jack": "07483806453",
    "Elliot": "07949104752",
    "Connor": "07944731036",
    "Ben": "07763439896",
    "Ben 2": "07414279228",
  };

  const handleEngineerChange = (e: any) => {
    setSelectedEngineer(e.target.value);
    setClientDetails({
      ...clientDetails,
      engineerNumber: engineerPhoneNumbers[e.target.value],
    });
  };

  const handlePlumberChange = (e: any) => {
    setSelectedPlumber(e.target.value);
    if (e.target.value === 'Elliot the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07949104752',
      });
    } else if (e.target.value === 'Connor the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07944731036',
      });
    } else if (e.target.value === 'Jack the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07483806453',
      });
    } else if (e.target.value === 'Ben the Plumber') {
        setClientDetails({
          ...clientDetails,
          contactNumber: '07763439896',
      });
    } else {
      setClientDetails({
        ...clientDetails,
        contactNumber: '',
      });
    }
  };

  const handleCustomerFound = (e: any) => {
    setCustomerFound(e.target.value);
  };

  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      contactNumber: e.target.value,
    });
  };

  const handleTextAreaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newDetails = {...details};
    newDetails['labour']['description'] = e.target.value;
    setDetails(newDetails);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>, type: 'labour' | 'material') => {
    const newDetails = {...details};
    newDetails[type][e.target.name as 'description' | 'quantity' | 'cost'] = e.target.value;
    setDetails(newDetails);
  };

  const calculateCosts = () => {
    let totalCost = Object.values(details).reduce((total, detail) => total + Number(detail.cost) * Number(1), 0);
    return { totalCost };
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (!details.labour.description || !unableToGuarantee) {
      alert('Please fill in all required fields.');
      return;
    }

    let newId;
    try {
      const metadataRef = doc(db, 'metadata', 'latestProductId');
      await runTransaction(db, async (transaction) => {
        const metadataSnap = await transaction.get(metadataRef);
        let maxId = 0;
        if (metadataSnap.exists()) {
          maxId = metadataSnap.data().id;
        } else {
          await setDoc(metadataRef, { id: maxId });
        }
        newId = (maxId + 1).toString();
        transaction.update(metadataRef, { id: parseInt(newId) });
        const date = new Date().toLocaleDateString('en-GB', {
          day: 'numeric',
          month: 'long',
          year: 'numeric'
        });
        const newProduct = {
          id: newId,
          title: clientDetails.clientName,
          price: totalCost,
          description: '',
          category: productCategory,
          image: '',
          quantity: 1,
          clientDetails,
          details: Object.values(details),
          engineer: selectedEngineer,
          unableToGuarantee,
          reason,
          customer: customerFound,
          date: date,
        };
        await addDoc(collection(db, 'products'), newProduct);
      });
      const lastUpdatedRef = collection(db, 'lastUpdated');
      const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
      const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
      const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
      if (lastUpdatedDocRef) {
        await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
      } else {
        console.error('Last updated document not found');
      }
      let engineer;

      if(selectedEngineer == "Ben 2")
      {
        engineer = "Ben";
      }
      else
      {
        engineer = selectedEngineer;
      }

      const reviewMessage = `Hi,\n\nI hope you're happy after ${engineer} the Plumber's visit?\n\nIf so, please leave a review here -\n\nhttps://the-plumber.co.uk/leave-a-review/\n\nIf you have any questions or concerns, don't hesitate to message me on here\n\n(avg response time: 3 mins)\n\nJack\nThe Plumber`;
      await axios.post('/.netlify/functions/text-magic-request-fifteen-minutes', {
        text: reviewMessage,
        phones: "+44" + clientDetails.contactNumber,
      }).then(() => {
        console.log('Review message scheduled');
      }).catch((error) => {
        console.error('An error occurred while scheduling the review message:', error);
      });
      
    } catch (error) {
        console.error("An error occurred:", error);
    } finally {
      try {
          let engineer;
    
          if(selectedEngineer == "Ben 2")
          {
            engineer = "Ben";
          }
          else
          {
            engineer = selectedEngineer;
          }
          
          const quoteDetails = `Hi,\n\nPlease see ${engineer} the Plumber's report/invoice -\n\nhttps://trade-safe.netlify.app/receipts/${newId}\n\nJack\nThe Plumber\n\nLeave a review - https://the-plumber.co.uk/leave-a-review/`;
          await axios.post('/.netlify/functions/text-magic-request', {
              text: quoteDetails,
              phones: "+44" + clientDetails.engineerNumber + "," + "+44" + clientDetails.contactNumber,
          });
      } catch (error) {
          console.error("An error occurred while sending the text:", error);
      }
      setSuccessMessage('Invoice sent successfully!');
    }
  };

  // Handle review button click
  const handleReviewClick = (response: 'Yes' | 'No') => {
    setReviewOptionSelected(true);
    if (response === 'Yes') {
      setReviewScheduled(true);
    }
  };

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <HeaderPlumber /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Invoice</span></h1>
        <h2 style={{ color: '#209ee6', textAlign: 'center', fontSize: '20px', fontWeight: 'bold' }}>Paid</h2>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>          
          {/* Customer Mobile Section */}
          <input type="tel" name="contactNumber" value={clientDetails.contactNumber} onChange={handleClientInputChange} placeholder="Customer Mobile" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} required/>
          {/* Plumber Selection */}
          {/* <select name="plumber" onChange={handlePlumberChange} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}>
          <option value="Elliot the Plumber">Elliot the Plumber</option>
          <option value="Connor the Plumber">Connor the Plumber</option>
          <option value="Jack the Plumber">Jack the Plumber</option>
          <option value="Ben the Plumber">Ben the Plumber</option>
          </select> */}
          <br />
          <br />
          <div>
            <strong>Engineer:</strong>
            <select value={selectedEngineer} onChange={handleEngineerChange} required>
              <option value="" disabled>Select Engineer</option>
              <option value="Jack">Jack - 07483806453</option>
              <option value="Elliot">Elliot - 07949104752</option>
              <option value="Connor">Connor - 07944731036</option>
              <option value="Ben">Ben - 07763439896</option>
              <option value="Ben 2">Ben 2 - 07414279228</option>
            </select>
          </div>
          <br />
          {/* Remove 1 Year Guarantee Section */}
          {/* Labour and Material Details Section */}
          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <textarea name="description" value={details.labour.description} onChange={handleTextAreaChange} placeholder={`Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} required />
            <br />
            {/* Unable to Guarantee Section */}
            <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
              <br />
              <textarea placeholder={`Unable To Guarantee`} name="unableToGuarantee" value={unableToGuarantee} onChange={(e) => setUnableToGuarantee(e.target.value)} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} required />
            </div>
            <br />
            <strong>Labour</strong>
            <div style={{ display:'flex', justifyContent:'space-between' }}>
              {/* <input type="number" name="quantity" value="1" onChange={(e) => handleInputChange(e, 'labour')} placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} readOnly/> */}
              <input type="number" name="cost" value={details.labour.cost} onChange={(e) => handleInputChange(e, 'labour')} placeholder={`Unit Price (£)`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          </div>

          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <strong>Materials</strong>
            <div style={{ display:'flex', justifyContent:'space-between' }}>
              {/* <input type="number" name="quantity" value="1" onChange={(e) => handleInputChange(e, 'material')} placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} readOnly/> */}
              <input type="number" name="cost" value={details.material.cost} onChange={(e) => handleInputChange(e, 'material')} placeholder={`Unit Price (£)`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          </div>

          {/* Display totalCost and Total */}
          <div style={{ marginTop:'10px' }}>
            <br />
            <strong>Total</strong>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCost.toFixed(2)}</div>
          </div>
          <br />
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/orangesendbutton.png" alt="Submit" />
            </button>
          </div>
          <p>{successMessage}</p>
        </form>
      </div>
    </div>
  );  
};

export default ReceiptForm;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
  ) => {
      const session = await getSession(context).catch((error) => {
        console.error('Error getting session:', error);
        return null;
      });

      return {
      props: {
      session,
    },
  };
};