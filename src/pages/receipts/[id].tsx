import HeaderPlumber from 'components/HeaderPlumber';
import { GetServerSidePropsContext } from "next";
import { useRouter } from 'next/router';
import { getSession, useSession } from "next-auth/react";
import { IProduct, ISession } from '../../../typings';
import axios from 'axios';
import { Stripe, loadStripe } from '@stripe/stripe-js';
import FooterQuestions from 'components/FooterQuestions';
let stripePromise: Promise<Stripe | null>;
import React, { useEffect, useState } from 'react';
import { collection, getDocs, query, where } from 'firebase/firestore';
import db from '../../../firebase';

const ReceiptsPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: session } = useSession();
  const [product, setProduct] = useState<IProduct | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [showGuaranteeYesInfo, setShowGuaranteeYesInfo] = useState(false);
  const [showGuaranteeNoInfo, setShowGuaranteeNoInfo] = useState(false);
  let totalAmount: any;

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        // Check if the product is in the cache
        const cachedProduct = localStorage.getItem(`product-${id}`);
        if (cachedProduct) {
          setProduct(JSON.parse(cachedProduct) as IProduct);
          setLoading(false);
          return;
        }
  
        const productsRef = collection(db, 'products');
        const q = query(productsRef, where("id", "==", id));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const productDoc = querySnapshot.docs[0];
          const productData = { ...productDoc.data(), id: productDoc.id } as IProduct;
          setProduct(productData);
  
          // Cache the product
          localStorage.setItem(`product-${id}`, JSON.stringify(productData));
        } else {
          throw new Error('Product not found');
        }
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };    

    fetchProduct();
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!product) {
    return <div><HeaderPlumber /><div>Invoice not found</div></div>;
  }

  const details = product!.details;

  const calculateCosts = () => {
    let totalCost = details.reduce((total: any, detail: any) => total + Number(detail.cost) * Number(detail.quantity), 0);

    totalAmount = totalCost;
    
    return { totalCost };
  };

  const handlePayButtonClick = async () => {
    if (!stripePromise) {
      stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY!);
    }

    const data = {
      items: [{
        price: totalAmount,
        title: "Quote",
        quantity: 1,
        id: product.id,
      }],
      email: session?.user?.email || '',
      quote_id: id,
      contactNumber: product.clientDetails.contactNumber,
      paymentMethod: 'stripe',
    };
  
    try {
      const response = await axios.post('/.netlify/functions/create-checkout-session', data);
  
      const stripe = await stripePromise;
      if (stripe) {
        await stripe.redirectToCheckout({ sessionId: response.data.id });
      } else {
        console.error('Stripe failed to load');
      }
    } catch (error) {
      console.error('Error creating checkout session:', error);
    }
  };

  return (
    <div>
      <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto', paddingBottom: '15em' }}>
        <HeaderPlumber /> {/* Include the HeaderPlumber */}
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px', marginBottom: '10px' }}>
          <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Invoice</span></h1>
          <h2 style={{ color: '#209ee6', textAlign: 'center', fontSize: '20px', fontWeight: 'bold' }}>Paid</h2>
          <form style={{ width: '100%' }}>
            <p>Engineer:</p>
            <div style={{ backgroundColor: '#D3D3D3', padding: '10px' }}>{product?.engineer || 'N/A'}</div>
            <br />
            <p>Date:</p>
            <div style={{ backgroundColor: '#D3D3D3', padding: '10px' }}>{product?.date || 'N/A'}</div>
            <br />
            <p>Description:</p>
            <br />
            <div tabIndex={0} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', border: '2px solid transparent', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap', overflow: 'auto' }} >
              <p>{details[0]?.description}</p>
            </div>
            {/* Unable to Guarantee Section */}
            <p>Unable to Guarantee:</p>
            <br />
            <div tabIndex={0} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', border: '2px solid transparent', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap', overflow: 'auto' }} >
              <p>{product.unableToGuarantee}</p>
            </div>
            {/* Display Total */}
            <div style={{ marginTop:'10px' }}>
              <strong>Labour</strong>
              <br />
              <p>Cost (£):</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[0]?.cost}</div>
              <br />
              <strong>Materials</strong>
              <br />
              <p>Cost (£):</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[1]?.cost}</div>
            </div>
            <br />
            <strong>Total</strong>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCost.toFixed(2)}</div>
            <br />
          </form>
        </div>
        <br />
        <div style={{ textAlign: 'center' }}>
          <div>
            <p>All work is covered by a 1 year guarantee <a className="text-blue-500 underline hover:text-blue-700" href="https://theplumber.ltd/terms-conditions/">T&C’s</a> apply</p>
          </div>
        </div>
      </div>
      <FooterQuestions />
    </div>
  );
};

export default ReceiptsPage;

export const getServerSideProps = async (
    context: GetServerSidePropsContext
) => {
  const session: ISession | null = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};