import React, { useEffect, useState } from 'react';
import { collection, getDocs } from 'firebase/firestore';
import { useRouter } from 'next/router';
import { IProduct } from '../../../typings';
import Header from '../../components/Header';
import ProductFeed from '../../components/ProductFeed';
import db from '../../../firebase';

const CategoryPage = () => {
  const router = useRouter();
  const { category: categorySegments } = router.query;
  const [products, setProducts] = useState<IProduct[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  // Get the category, subcategory, and subsubcategory values from the URL path
  const [category, subcategory, subsubcategory] = categorySegments || [];

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const productsRef = collection(db, 'products');
        const productSnap = await getDocs(productsRef);

        const allProducts: IProduct[] = productSnap.docs.map(doc => doc.data() as IProduct);
        setProducts(allProducts);
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };

    fetchProducts();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  // Filter the list of products based on the category, subcategory, and subsubcategory
  const filteredProducts = products.filter(product => {
    if (subsubcategory) {
      return product.subsubcategory === subsubcategory && product.subcategory === subcategory && product.category === category;
    } else if (subcategory) {
      return product.subcategory === subcategory && product.category === category;
    } else {
      return product.category === category;
    }
  });

  return (
    <div className="bg-gray-100">
      <Header />
      <main className="max-w-screen-2xl mx-auto">
        <h1 className="text-3xl font-bold mt-4 mb-6 text-center">{subsubcategory || subcategory || category}</h1>
        {/* Pass the filtered list of products to the ProductFeed component */}
        <ProductFeed products={filteredProducts} />
      </main>
    </div>
  );
};

export default CategoryPage;