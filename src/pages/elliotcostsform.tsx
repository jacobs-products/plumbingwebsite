import { useState } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderElliot from '../components/HeaderElliot';
import { addDoc, collection, doc, getDocs, limit, orderBy, query, runTransaction, serverTimestamp, setDoc, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const ElliotCostsForm = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '07949104752',
  });

  const [details, setDetails] = useState({
    labour: { type: 'Labour', description: '', quantity: '1', cost: '' },
    material: { type: 'Material', description: '', quantity: '1', cost: '' }
  });

  const [successMessage, setSuccessMessage] = useState('');

  const [totalCost, setTotalCost] = useState(49);
  const [productCategory, setProductCategory] = useState('');
  const [selectedPlumber, setSelectedPlumber] = useState('Elliot the Plumber');

  const handlePlumberChange = (e: any) => {
    setSelectedPlumber(e.target.value);
    if (e.target.value === 'Elliot the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07949104752',
      });
    } else if (e.target.value === 'Jack the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07483806453',
      });
    } else {
      setClientDetails({
        ...clientDetails,
        contactNumber: '',
      });
    }
  };  

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      [e.target.name]: e.target.value,
    });
  };

  const handleTextAreaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newDetails = {...details};
    newDetails['labour']['description'] = e.target.value;
    setDetails(newDetails);
  };

  // Handle input change for labour and material details
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>, type: 'labour' | 'material') => {
    const newDetails = {...details};
    newDetails[type][e.target.name as 'description' | 'quantity' | 'cost'] = e.target.value;
    setDetails(newDetails);
  };

  // Calculate total cost
  const calculateCosts = () => {
    let totalCost = Object.values(details).reduce((total, detail) => total + Number(detail.cost) * Number(1), 0); //detail.quantity is just 1 instead
    return { totalCost };
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let newId;
    try {
      // Reference to the document that holds the latest ID
      const metadataRef = doc(db, 'metadata', 'latestProductId');
  
      // Use a transaction to ensure the operation is atomic
      await runTransaction(db, async (transaction) => {
        const metadataSnap = await transaction.get(metadataRef);
        let maxId = 0;
  
        if (metadataSnap.exists()) {
          maxId = metadataSnap.data().id;
        } else {
          // Create the metadata document if it doesn't exist
          await setDoc(metadataRef, { id: maxId });
        }
  
        // Generate a new ID based on the latest ID
        newId = (maxId + 1).toString();
  
        // Update the latest ID in the metadata document
        transaction.update(metadataRef, { id: parseInt(newId) });
  
        // Prepare the new product data
        const newProduct = {
          id: newId,
          title: clientDetails.clientName,
          price: totalCost,
          description: '',
          category: productCategory,
          image: '',
          quantity: 1,
          clientDetails,
          details: Object.values(details),
          distribution: {
            labour: { elliot: 0.51, jack: 0.49 },
            material: { elliot: 0.75, jack: 0.25 }
          }
        };
  
        // Add the new product document
        await addDoc(collection(db, 'products'), newProduct);
      });

      const lastUpdatedRef = collection(db, 'lastUpdated');
      const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
      const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
      const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
      if (lastUpdatedDocRef) {
        await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
      } else {
        console.error('Last updated document not found');
      }

    } catch (error) {
        console.error("An error occurred:", error);
    } finally {
        try {
            const quoteDetails = `https://trade-safe.netlify.app/elliotcosts/${newId}`;

            await axios.post('/.netlify/functions/text-magic-request', {
                text: quoteDetails,
                phones: "+44" + clientDetails.contactNumber,
            });
        } catch (error) {
            console.error("An error occurred while sending the text:", error);
        }

        setSuccessMessage('Costs sent successfully!');
    }
  }

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <HeaderElliot /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Costs</span></h1>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>
          {/* Plumber Selection */}
          <select name="plumber" onChange={handlePlumberChange} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}>
          <option value="Elliot the Plumber">Elliot the Plumber</option>
          <option value="Jack the Plumber">Jack the Plumber</option>
          </select>
          {/* Labour and Material Details Section */}
          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <textarea name="description" value={details.labour.description} onChange={handleTextAreaChange} placeholder={`Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} />
            <br />
            <strong>Labour</strong>
            <div style={{ display:'flex', justifyContent:'space-between' }}>
              {/* <input type="number" name="quantity" value="1" onChange={(e) => handleInputChange(e, 'labour')} placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} readOnly/> */}
              <input type="number" name="cost" value={details.labour.cost} onChange={(e) => handleInputChange(e, 'labour')} placeholder={`Unit Price (£)`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          </div>

          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <strong>Materials</strong>
            <div style={{ display:'flex', justifyContent:'space-between' }}>
              {/* <input type="number" name="quantity" value="1" onChange={(e) => handleInputChange(e, 'material')} placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} readOnly/> */}
              <input type="number" name="cost" value={details.material.cost} onChange={(e) => handleInputChange(e, 'material')} placeholder={`Unit Price (£)`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          </div>

          {/* Display totalCost and Total */}
          <div style={{ marginTop:'10px' }}>
            <br />
            <strong>Total</strong>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCost.toFixed(2)}</div>
          </div>
          <br />
          {/* Submit Button */}
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/orangesendbutton.png" alt="Submit" />
            </button>
          </div>
          <p>{successMessage}</p>
        </form>
      </div>
    </div>
  );  
};

export default ElliotCostsForm;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
  ) => {
      const session = await getSession(context).catch((error) => {
        console.error('Error getting session:', error);
        return null;
      });

      return {
      props: {
      session,
    },
  };
};