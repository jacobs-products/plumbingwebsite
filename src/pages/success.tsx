import HeaderGuarantee from 'components/HeaderGuarantee';
import Footer from 'components/Footer';
import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import { ISession } from '../../typings';

const Success = () => {    
  return (
    <div>
      <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto', paddingBottom: '15em' }}>
        <HeaderGuarantee /> {/* Include the HeaderGuarantee */}
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px', marginBottom: '10px' }}>
          <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Thanks for your payment</span></h1>
          <br />
          <p>A payment to THE PLUMBER will appear on your statement.</p>
        </div>
        <br />
        <div style={{ textAlign: 'center' }}>
          <strong>Terms -</strong>
          <p>All work is covered by a 1 year guarantee <a className="text-blue-500 underline hover:text-blue-700" href="https://theplumber.ltd/terms-conditions/">T&C’s</a></p>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Success;

export const getServerSideProps = async (
    context: GetServerSidePropsContext
) => {
  const session: ISession | null = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};