import { useState, useEffect } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderBen from '../components/HeaderBen';
import { addDoc, collection, doc, getDocs, limit, orderBy, query, runTransaction, serverTimestamp, setDoc, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const MaterialsQuoteForm3 = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '',
  });

  const [details, setDetails] = useState<Array<{ type: string, description: string, quantity: string, cost: string }>>([]); 

  const [successMessage, setSuccessMessage] = useState('');

  const [link, setLink] = useState("https://trade-safe.co.uk/a50/");
  const [totalCost, setTotalCost] = useState(10);
  const [productCategory, setProductCategory] = useState('');
  const [selectedPlumber, setSelectedPlumber] = useState('Ben the Plumber');

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      contactNumber: e.target.value,
    });
  };

  // Handle input change for labour details
  const handleCostChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if(e.target.value)
    {
      setTotalCost(parseInt(e.target.value));
      const selectElement = document.querySelector('select[name="cost"]');
      if (selectElement) {
        const selectedIndex = (selectElement as HTMLSelectElement).selectedIndex;
        const options = (selectElement as HTMLSelectElement).options;
        let foundFirstIteration = false;
        for (let i = 0; i < options.length; i++) {
          if (options[i].value === e.target.value) {
            if (foundFirstIteration && i === selectedIndex) {
              setProductCategory('secondversion');
              break;
            }
            if (!foundFirstIteration) {
              foundFirstIteration = true;
            }
          }
        }
      }
    }
  };

  const handleDetailsChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const newDetails = [...details];
    const key = e.target.name as keyof typeof newDetails[0];
    newDetails[index][key] = e.target.value;
    setDetails(newDetails);
  };

  // Add a new labour detail section
  const addLabourDetailSection = () => {
    setDetails([...details, {
      type: 'Labour',
      description: '',
      quantity: '',
      cost: '',
    }]);
  };

  // Handle delete operation for labour details
  const handleDelete = (index: number) => {
    const newDetails = [...details];
    newDetails.splice(index, 1);
    setDetails(newDetails);
  }

  // Handle plumber selection
  const handlePlumberChange = (e: any) => {
    setSelectedPlumber(e.target.value);
    if (e.target.value === 'Ben the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07763439896',
      });
    } else if (e.target.value === 'Jack the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07483806453',
      });
    } else if (e.target.value === 'Elliot the Plumber') {
      setClientDetails({
        ...clientDetails,
        contactNumber: '07949104752',
      });
    } else {
      setClientDetails({
        ...clientDetails,
        contactNumber: '',
      });
    }
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let newId;
    try {
      // Reference to the document that holds the latest ID
      const metadataRef = doc(db, 'metadata', 'latestProductId');
  
      // Use a transaction to ensure the operation is atomic
      await runTransaction(db, async (transaction) => {
        const metadataSnap = await transaction.get(metadataRef);
        let maxId = 0;
  
        if (metadataSnap.exists()) {
          maxId = metadataSnap.data().id;
        } else {
          // Create the metadata document if it doesn't exist
          await setDoc(metadataRef, { id: maxId });
        }
  
        // Generate a new ID based on the latest ID
        newId = (maxId + 1).toString();
  
        // Update the latest ID in the metadata document
        transaction.update(metadataRef, { id: parseInt(newId) });
  
        // Prepare the new product data
        const newProduct = {
          id: newId,
          title: clientDetails.clientName,
          price: totalCost,
          description: '',
          category: productCategory,
          image: '',
          quantity: 1,
          clientDetails,
          details,
        };
  
        // Add the new product document
        await addDoc(collection(db, 'products'), newProduct);
      });

      const lastUpdatedRef = collection(db, 'lastUpdated');
      const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
      const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
      const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
      if (lastUpdatedDocRef) {
        await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
      } else {
        console.error('Last updated document not found');
      }

    } catch (error) {
        console.error("An error occurred:", error);
    } finally {
        try {
          const quoteDetails = `Hi,\n\nPlease approve our material costs -\n\nhttps://trade-safe.netlify.app/materialsquotes3/${newId}\n\nThe Plumber\n01322 771767\nwww.the-plumber.co.uk`;

            await axios.post('/.netlify/functions/text-magic-request', {
                text: quoteDetails,
                phones: "+44" + clientDetails.contactNumber,
            });
        } catch (error) {
            console.error("An error occurred while sending the text:", error);
        }

        setSuccessMessage('Invoice sent successfully!');
    }
  }

  // Handle input change for description
  const handleDescriptionChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDetails([{ type: 'Labour', description: e.target.value, quantity:"1", cost:"10" }]);
  };

  
return (
  <div style={{ justifyContent:'center', alignItems:'center', fontFamily:'Arial,sans-serif', width:'75%', margin:'0 auto' }}>
    <HeaderBen /> {/* Include the header */}
    <div style={{ display:'flex', flexDirection:'column', alignItems:'center', padding:'5px', width:'100%', border:'2px solid #209ee6', marginTop:'100px' }}>
      <h1 style={{ color:'#000000', textAlign:'center', fontSize:'40px', fontWeight:'bold' }}><span style={{ borderBottom:'4px solid #209ee6' }}>Materials</span></h1>
      <form onSubmit={handleSubmit} style={{ width:'100%' }}>
        {/* Customer Mobile Section */}
        <input type="tel" name="contactNumber" value={clientDetails.contactNumber} onChange={handleClientInputChange} placeholder="Customer Mobile" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} required/>
        {/* Labour and Material Details Section */}
        <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
          <br />
          <textarea name="quote" onChange={handleDescriptionChange} placeholder={`Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} />
          <div>
            <br />
            <p>Materials Cost (£):</p>
            <select name="cost" onChange={handleCostChange} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}>
              <option value="10" selected>£10</option>
              <option value="20">£20</option>
              <option value="30">£30</option>
              <option value="40">£40</option>
              <option value="50">£50</option>
              <option value="60">£60</option>
              <option value="70">£70</option>
              <option value="80">£80</option>
              <option value="90">£90</option>
              <option value="100">£100</option>
            </select>
          </div>
        </div>

        {/* Display totalCost and Total */}
        <div style={{ marginTop:'10px' }}>
          <p>Materials Total:</p>
          <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{totalCost.toFixed(2)}</div>
        </div>
        <br />
        {/* Submit Button */}
        <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
          <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
          <img src="/orangesendbutton.png" alt="approve" />
          </button>
        </div>
        <p>{successMessage}</p>
      </form>
    </div>
  </div>
  );  
};

export default MaterialsQuoteForm3;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
  ) => {
      const session = await getSession(context).catch((error) => {
        console.error('Error getting session:', error);
        return null;
      });

      return {
      props: {
      session,
    },
  };
};