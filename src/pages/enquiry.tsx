import { useState } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderGuarantee from '../components/HeaderGuarantee';

const Enquiry = () => {
  const [clientDetails, setClientDetails] = useState({
    contactNumber: '',
    job: '',
  });

  const [leadSource, setLeadSource] = useState('');
  const [chase, setChase] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [chaseMessage, setChaseMessage] = useState('');

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    const { name, value } = e.target;
    setClientDetails((prevDetails) => ({
      ...prevDetails,
      [name]: value,
    }));
  };

  const handleLeadSourceChange = (e: any) => {
    setLeadSource(e.target.value);
  };

  const handleChaseChange = (e: any) => {
    setChase(e.target.value);
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    // Check if all required fields are filled
    if (!clientDetails.contactNumber || !clientDetails.job) {
      alert('Please fill in all the required fields.');
      return;
    }

    try {

      const message = `Hi,\n\nThanks for your enquiry!\n\nSee our costs & book here -\n\nThePlumber.co.uk/book\n\nPlease let me know if you have any questions\n\nJack\nThe Plumber`;

      // Send the message
      await axios.post('/.netlify/functions/text-magic-request', {
        text: message,
        phones: "+44" + clientDetails.contactNumber,
      });

      // If chase is yes, schedule follow-up message
      if (chase === 'yes') {
        // Schedule follow-up message
        const message2 = `Hi,\n\nIf you have any questions, you can reply to this message\n\n(avg response time: 3 mins)\n\nor\n\nYou can book here whenever you're ready -\n\nThePlumber.co.uk/book\n\nJack\nThe Plumber`

      // Send the message
      await axios.post('/.netlify/functions/text-magic-request-fifteen-minutes', {
        text: message2,
        phones: "+44" + clientDetails.contactNumber,
      });
      
      setChaseMessage('Chase message will be scheduled in 15 minutes!');
      }

      setSuccessMessage('Message sent successfully!');
    } catch (error) {
      console.error('An error occurred while sending the text:', error);
    }
  };

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <HeaderGuarantee /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}>
          <span style={{ borderBottom: '4px solid #209ee6' }}>Enquiry</span>
        </h1>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>
          {/* Customer Mobile Section */}
          <input
            type="tel"
            name="contactNumber"
            value={clientDetails.contactNumber}
            onChange={handleClientInputChange}
            placeholder="Customer Mobile"
            style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}
            required
          />
          {/* Job Section */}
          <input
            type="text"
            name="job"
            value={clientDetails.job}
            onChange={handleClientInputChange}
            placeholder="job(s)"
            style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }}
            required
          />
          {/* Chase Section */}
          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <p>Chase*:</p>
            <div>
              <input type="radio" id="chaseYes" name="chase" value="yes" onChange={handleChaseChange} required />
              <label htmlFor="chaseYes">Yes</label><br />
              <input type="radio" id="chaseNo" name="chase" value="no" onChange={handleChaseChange} required />
              <label htmlFor="chaseNo">No</label><br />
            </div>
          </div>
          <br />
          {/* Submit Button */}
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/orangesendbutton.png" alt="approve" />
            </button>
          </div>
          <p>{successMessage}</p>
          <br />
          <p>{chaseMessage}</p>
        </form>
      </div>
    </div>
  );
};

export default Enquiry;

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
  const session = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};