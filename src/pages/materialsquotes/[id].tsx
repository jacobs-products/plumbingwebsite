import HeaderElliot from 'components/HeaderElliot';
import { GetServerSidePropsContext } from "next";
import { useRouter } from 'next/router';
import { getSession, useSession } from "next-auth/react";
import { IProduct, ISession } from '../../../typings';
import Footer from 'components/Footer';
import React, { useEffect, useState } from 'react';
import { collection, doc, getDoc, getDocs, query, where } from 'firebase/firestore';
import db from '../../../firebase';

const MaterialsQuotePage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: session } = useSession();
  const [product, setProduct] = useState<IProduct | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        // Check if the product is in the cache
        const cachedProduct = localStorage.getItem(`product-${id}`);
        if (cachedProduct) {
          setProduct(JSON.parse(cachedProduct) as IProduct);
          setLoading(false);
          return;
        }
  
        const productsRef = collection(db, 'products');
        const q = query(productsRef, where("id", "==", id));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const productDoc = querySnapshot.docs[0];
          const productData = { ...productDoc.data(), id: productDoc.id } as IProduct;
          setProduct(productData);
  
          // Cache the product
          localStorage.setItem(`product-${id}`, JSON.stringify(productData));
        } else {
          throw new Error('Product not found');
        }
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };

    fetchProduct();
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!product) {
    return <div><HeaderElliot /><div>Invoice not found</div></div>;
  }

  const details = product!.details;
  
  const costToLinkMap = {
    10: "https://buy.stripe.com/eVa5mycrwgzCbmgeVC",
    20: "https://buy.stripe.com/fZe9COdvA3MQduoaFn",
    30: "https://buy.stripe.com/fZe7uG638gzC3TO5l4",
    40: "https://buy.stripe.com/00geX88bg1EI1LGdRB",
    50: "https://buy.stripe.com/eVa02efDIeru4XSeVG",
    60: "https://buy.stripe.com/00gg1c1MS836gGA3cZ",
    70: "https://buy.stripe.com/00gg1cfDI6Z2aicfZM",
    80: "https://buy.stripe.com/aEUg1cgHMdnqeysaFt",
    90: "https://buy.stripe.com/cN202ecrw3MQduofZO",
    100: "https://buy.stripe.com/eVabKW2QWeruaic8xn"
  };  

  const handleButtonClick = (e: any) => {
    if(product) {
      e.preventDefault();
      let link = costToLinkMap[(product as IProduct)?.price as keyof typeof costToLinkMap];
      // use this code if there are multiple versions with the same price
      // if (product && product.category === 'secondversion') {
      //   switch ((product as IProduct).price) {
      //     case 49:
      //       link = "https://trade-safe.co.uk/a68";
      //       break;
      //     case 99:
      //       link = "https://trade-safe.co.uk/a70";
      //       break;
      //     case 120:
      //       link = "https://trade-safe.co.uk/a72";
      //       break;
      //   }
      // }
      if (link) {
        window.location.href = link;
      } else {
        console.error('Invalid product price:', (product as IProduct).price);
      }
  };
}

  return (
    <div>
      <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto', paddingBottom: '15em' }}>
        <HeaderElliot />
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
          <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Materials</span></h1>
          <form style={{ width: '100%' }}>
            <br />
            <p>Description:</p>
            <br />
            <div tabIndex={0} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', border: '2px solid transparent', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap', overflow: 'auto' }} >
              <p>{details[0]?.description}</p>
            </div>
            {/* Display Total */}
            <div style={{ marginTop:'10px' }}>
              <p>Materials Total:</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{product.price.toFixed(2)}</div>
            </div>
            <br />
              {/* Submit Button */}
              <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto" }}>
                  <button onClick={handleButtonClick} style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
                      <img src="/orangeapprovepaybutton.png" alt="approve" />
                  </button>
              </div>
          </form>
        </div>
        <br />
        <div style={{ textAlign: 'center' }}>
          <strong>Terms -</strong>
          <p>Materials are covered by manufacturers warranty <a className="text-blue-500 underline hover:text-blue-700" href="https://theplumber.ltd/terms-conditions/">T&C’s</a></p>
        </div>
      </div>
      <Footer />
    </div>

  );
};

export default MaterialsQuotePage;

export const getServerSideProps = async (
    context: GetServerSidePropsContext
) => {
  const session: ISession | null = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};