import { useState } from 'react';
import axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import HeaderPlumber from '../components/HeaderPlumber';
import { addDoc, collection, doc, getDocs, limit, orderBy, query, runTransaction, serverTimestamp, setDoc, updateDoc, where } from 'firebase/firestore';
import db from '../../firebase';
import { IProduct } from '../../typings';

const CostsForm = () => {
  const [clientDetails, setClientDetails] = useState({
    clientName: '',
    street: '',
    city: '',
    county: '',
    postcode: '',
    contactNumber: '',
    engineerNumber: '',
  });

  const [details, setDetails] = useState({
    labour: { type: 'Labour', description: '', quantity: '1', cost: '' },
    material: { type: 'Material', description: '', quantity: '1', cost: '' },
    labourCash: { type: 'Labour (Cash)', description: '', quantity: '1', cost: '' },
    materialCash: { type: 'Material (Cash)', description: '', quantity: '1', cost: '' }
  });

  const [successMessage, setSuccessMessage] = useState('');
  const [totalCost, setTotalCost] = useState(49);
  const [productCategory, setProductCategory] = useState('');
  const [selectedPlumber, setSelectedPlumber] = useState('Jack the Plumber');
  const [fixedQuote, setFixedQuote] = useState('');
  const [selectedEngineer, setSelectedEngineer] = useState('');
  const [cashPrice, setCashPrice] = useState('');
  const [unableToGuarantee, setUnableToGuarantee] = useState('');
  const [infoMessage, setInfoMessage] = useState('');

  const engineerPhoneNumbers: any = {
    "Jack": "07483806453",
    "Elliot": "07949104752",
    "Connor": "07944731036",
    "Ben": "07763439896",
    "Ben 2": "07414279228",
  };

  const handleEngineerChange = (e: any) => {
    setSelectedEngineer(e.target.value);
    setClientDetails({
      ...clientDetails,
      engineerNumber: engineerPhoneNumbers[e.target.value],
    });
  };

  // Handle input change for client details
  const handleClientInputChange = (e: any) => {
    setClientDetails({
      ...clientDetails,
      contactNumber: e.target.value,
    });
  };

  const handleTextAreaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newDetails = { ...details };
    newDetails['labour']['description'] = e.target.value;
    setDetails(newDetails);
  };

  // Handle input change for labour and material details
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>, type: 'labour' | 'material' | 'labourCash' | 'materialCash') => {
    const newDetails = { ...details };
    newDetails[type][e.target.name as 'description' | 'quantity' | 'cost'] = e.target.value;
    setDetails(newDetails);
  };

  // Calculate total cost
  const calculateCosts = () => {
    let totalCardCost = Object.values(details).reduce((total, detail) => {
      if (detail.type.includes('Cash')) return total;
      return total + Number(detail.cost) * Number(1);
    }, 0);
  
    let totalCashCost = Object.values(details).reduce((total, detail) => {
      if (!detail.type.includes('Cash')) return total;
      return total + Number(detail.cost) * Number(1);
    }, 0);
  
    return { totalCardCost, totalCashCost };
  };

  // Handle form submission
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    // Check if all required fields are filled
    if (!fixedQuote) {
      alert('Please select whether the quote is fixed or not.');
      return;
    }
    if (!cashPrice) {
      alert('Please select whether the cash price is applicable.');
      return;
    }
    if (!unableToGuarantee) {
      alert('Please provide the reason(s) the job is unable to be guaranteed.');
      return;
    }

    const { totalCardCost, totalCashCost } = calculateCosts();
  
    let newId;
    try {
      // Reference to the document that holds the latest ID
      const metadataRef = doc(db, 'metadata', 'latestProductId');

      // Use a transaction to ensure the operation is atomic
      await runTransaction(db, async (transaction) => {
        const metadataSnap = await transaction.get(metadataRef);
        let maxId = 0;

        if (metadataSnap.exists()) {
          maxId = metadataSnap.data().id;
        } else {
          // Create the metadata document if it doesn't exist
          await setDoc(metadataRef, { id: maxId });
        }

        // Generate a new ID based on the latest ID
        newId = (maxId + 1).toString();

        // Update the latest ID in the metadata document
        transaction.update(metadataRef, { id: parseInt(newId) });

        // Prepare the new product data
        const newProduct = {
          id: newId,
          title: clientDetails.clientName,
          price: totalCardCost,
          description: '',
          category: productCategory,
          image: '',
          quantity: 1,
          clientDetails,
          details: Object.values(details),
          distribution: {
            labour: { elliot: 0.51, jack: 0.49 },
            material: { elliot: 0.75, jack: 0.25 }
          },
          engineer: selectedEngineer,
          fixedQuote,
          cashPrice,
          unableToGuarantee,
        };

        // Add the new product document
        await addDoc(collection(db, 'products'), newProduct);
      });

      const lastUpdatedRef = collection(db, 'lastUpdated');
      const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
      const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
      const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
      if (lastUpdatedDocRef) {
        await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
      } else {
        console.error('Last updated document not found');
      }

    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      try {
        let engineer;
  
        if(selectedEngineer == "Ben 2")
        {
          engineer = "Ben";
        }
        else
        {
          engineer = selectedEngineer;
        }
    
        const quoteDetails = `Hi,\n\nPlease approve ${engineer} the Plumber's quote -\n\nhttps://trade-safe.netlify.app/costs/${newId}\n\nJack\nThe Plumber`;

        await axios.post('/.netlify/functions/text-magic-request', {
          text: quoteDetails,
          phones: "+44" + clientDetails.engineerNumber + "," + "+44" + clientDetails.contactNumber,
        });
      } catch (error) {
        console.error("An error occurred while sending the text:", error);
      }

      setSuccessMessage('Costs sent successfully!');
    }
  }

  const handleFixedQuoteChange = (e: any) => {
    setFixedQuote(e.target.value);
  };

  const handleCashPriceChange = (e: any) => {
    setCashPrice(e.target.value);
  };

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <HeaderPlumber />
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Costs</span></h1>
        <form onSubmit={handleSubmit} style={{ width: '100%' }}>
          <input type="tel" name="contactNumber" value={clientDetails.contactNumber} onChange={handleClientInputChange} placeholder="Customer Mobile" style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} required />
          <div>
            <br />
            <div>
              <strong>Engineer:</strong>
              <select value={selectedEngineer} onChange={handleEngineerChange} required>
                <option value="" disabled>Select Engineer</option>
                <option value="Jack">Jack - 07483806453</option>
                <option value="Elliot">Elliot - 07949104752</option>
                <option value="Connor">Connor - 07944731036</option>
                <option value="Ben">Ben - 07763439896</option>
                <option value="Ben 2">Ben 2 - 07414279228</option>
              </select>
            </div>
            <br />
            <strong>Fixed Quote*:</strong>
            <div>
              <input type="radio" id="yes" name="fixedQuote" value="1" onChange={handleFixedQuoteChange} required />
              <label htmlFor="yes">Yes</label>
              <br />
              <input type="radio" id="no" name="fixedQuote" value="2" onChange={handleFixedQuoteChange} required />
              <label htmlFor="no">No</label>
              {fixedQuote == "1" ? 
                <p>You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster</p> 
                  : fixedQuote == "2" ? 
                      <p>This is the minimum charge for this job and we will try our best to complete within this quoted amount. We may need more time, if so we’ll provide a quote for your approval before we proceed</p> 
                  : <p></p>
              }
            </div>
          </div>
          <br />
          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <textarea name="description" value={details.labour.description} onChange={handleTextAreaChange} placeholder={`Description`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} />
            <br />
            <label htmlFor="unableToGuarantee">Unable to Guarantee:</label>
            <br />
            <textarea placeholder={`Unable To Guarantee`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} id="unableToGuarantee" name="unableToGuarantee" value={unableToGuarantee} onChange={(e) => setUnableToGuarantee(e.target.value)} required />
            <br />
            <strong>Labour (Card)</strong>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <input type="number" name="cost" value={details.labour.cost} onChange={(e) => handleInputChange(e, 'labour')} placeholder={`Unit Price (£)`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          </div>
          <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
            <br />
            <strong>Materials (Card)</strong>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <input type="number" name="cost" value={details.material.cost} onChange={(e) => handleInputChange(e, 'material')} placeholder={`Unit Price (£)`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
            <br />
            <strong>Total (Card)</strong>
            <div style={{ backgroundColor: '#D3D3D3', padding: '10px' }}>£{calculateCosts().totalCardCost.toFixed(2)}</div>
          </div>
            <br />
            <div>
            <strong>Cash Price*:</strong>
            <div>
              <input type="radio" id="cashPriceYes" name="cashPrice" value="1" onChange={handleCashPriceChange} required />
              <label htmlFor="cashPriceYes">Yes</label>
              <br />
              <input type="radio" id="cashPriceNo" name="cashPrice" value="2" onChange={handleCashPriceChange} required />
              <label htmlFor="cashPriceNo">No</label>
            </div>
          </div>
          {cashPrice === '1' && (
            <div style={{ borderTop: '4px solid #000000', margin: '20px 0' }}>
              <div style={{ marginTop:'10px' }}>
                <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
                  <br />
                  <strong>Labour (Cash)</strong>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <input type="number" name="cost" value={details.labourCash.cost} onChange={(e) => handleInputChange(e, 'labourCash')} placeholder={`Unit Price (£)`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
                  </div>
                </div>
                <div style={{ position: 'relative', borderBottom: "1px solid #D3D3D3", paddingBottom: "10px" }}>
                  <br />
                  <strong>Materials (Cash)</strong>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <input type="number" name="cost" value={details.materialCash.cost} onChange={(e) => handleInputChange(e, 'materialCash')} placeholder={`Unit Price (£)`} style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '49%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
                </div>
                <br />
                <strong>Total (Cash)</strong>
                <div style={{ backgroundColor: '#D3D3D3', padding: '10px' }}>£{calculateCosts().totalCashCost.toFixed(2)}</div>
                </div>
              </div>
            </div>
          )}
          <br />
          <div style={{ marginTop: '10px' }}>
          </div>
          <br />
          <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto", textAlign: "center" }}>
            <button type="submit" style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
              <img src="/orangesendbutton.png" alt="Submit" />
            </button>
          </div>
          <p>{successMessage}</p>
        </form>
      </div>
    </div>
  );  
};

export default CostsForm;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
  ) => {
      const session = await getSession(context).catch((error) => {
        console.error('Error getting session:', error);
        return null;
      });

      return {
      props: {
      session,
    },
  };
};