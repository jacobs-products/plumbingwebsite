import HeaderBen from 'components/HeaderBen';
import { GetServerSidePropsContext } from "next";
import { useRouter } from 'next/router';
import { getSession, useSession } from "next-auth/react";
import { IProduct, ISession } from '../../../typings';
import Footer from 'components/Footer';
import React, { useEffect, useState } from 'react';
import { collection, doc, getDoc, getDocs, query, where } from 'firebase/firestore';
import db from '../../../firebase';

const CustomerQuotePage3 = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: session } = useSession();
  const [product, setProduct] = useState<IProduct | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [showFixedQuoteYesInfo, setShowFixedQuoteYesInfo] = useState(false);
  const [showFixedQuoteNoInfo, setShowFixedQuoteNoInfo] = useState(false);
  const [showHandymanYesInfo, setShowHandymanYesInfo] = useState(false);
  const [showHandymanNoInfo, setShowHandymanNoInfo] = useState(false);

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        // Check if the product is in the cache
        const cachedProduct = localStorage.getItem(`product-${id}`);
        if (cachedProduct) {
          setProduct(JSON.parse(cachedProduct) as IProduct);
          setLoading(false);
          return;
        }
  
        const productsRef = collection(db, 'products');
        const q = query(productsRef, where("id", "==", id));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const productDoc = querySnapshot.docs[0];
          const productData = { ...productDoc.data(), id: productDoc.id } as IProduct;
          setProduct(productData);
  
          // Cache the product
          localStorage.setItem(`product-${id}`, JSON.stringify(productData));
        } else {
          throw new Error('Product not found');
        }
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };

    fetchProduct();
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!product) {
    return <div><HeaderBen /><div>Quote not found</div></div>;
  }

  const details = product!.details;
  
  const costToLinkMap = {
    49: "https://buy.stripe.com/9AQbKWgHMgzC760eYd",
    98: "https://buy.stripe.com/14k4iu1MS2IM4XS7vN",
    147: "https://buy.stripe.com/5kA2am1MSabegGAaI0",
    196: "https://buy.stripe.com/eVa3eq0IOfvy1LG8zT",
    245: "https://buy.stripe.com/5kAg1c77c2IM61Wg2m",
    294: "https://buy.stripe.com/7sI7uG2QW0AEaic03p",
    343: "https://buy.stripe.com/4gwcP08bgabe0HCcQc",
    392: "https://buy.stripe.com/eVabKW2QW5UY61WcQd",
    490: "https://buy.stripe.com/bIY16icrwcjm3TObMa",
    588: "https://buy.stripe.com/fZecP0dvA5UY760eYn"
  };  

  const handleButtonClick = (e: any) => {
    if(product) {
      e.preventDefault();
      let link = costToLinkMap[(product as IProduct)?.price as keyof typeof costToLinkMap];
      // use this code if there are multiple versions with the same price
      // if (product && product.category === 'secondversion') {
      //   switch ((product as IProduct).price) {
      //     case 49:
      //       link = "https://trade-safe.co.uk/a68";
      //       break;
      //     case 99:
      //       link = "https://trade-safe.co.uk/a70";
      //       break;
      //     case 120:
      //       link = "https://trade-safe.co.uk/a72";
      //       break;
      //   }
      // }
      if (link) {
        window.location.href = link;
      } else {
        console.error('Invalid product price:', (product as IProduct).price);
      }
  };
}

const handleFixedQuoteInfoClick = (e: any, type: any) => {
  e.stopPropagation();
  if (type === 'yes' && product.fixedQuote == "1") {
    setShowFixedQuoteYesInfo(!showFixedQuoteYesInfo);
  } else if (type === 'no' && product.fixedQuote != "1") {
    setShowFixedQuoteNoInfo(!showFixedQuoteNoInfo);
  }
};

const handleHandymanInfoClick = (e: any, type: any) => {
  e.stopPropagation();
  if (type === 'yes' && product.handymanRequired == "1") {
    setShowHandymanYesInfo(!showHandymanYesInfo);
  } else if (type === 'no' && product.handymanRequired != "1") {
    setShowHandymanNoInfo(!showHandymanNoInfo);
  }
}

  return (
    <div>
      <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto', paddingBottom: '15em' }}>
        <HeaderBen />
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px' }}>
          <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Costs</span></h1>
          <form style={{ width: '100%' }}>
            <br />
            <p>Fixed Quote:</p>
            <br />
            <div>
              <input type="radio" id="yes" name="fixedQuote" value="1" checked={product.fixedQuote == '1'} readOnly />
              <label htmlFor="yes">Yes</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleFixedQuoteInfoClick(e, 'yes')}>i</button>
              {showFixedQuoteYesInfo && <p>You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster</p>}
            </div>
            <div>
              <input type="radio" id="no" name="fixedQuote" value="2" checked={product.fixedQuote == '2'} readOnly />
              <label htmlFor="no">No</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleFixedQuoteInfoClick(e, 'no')}>i</button>
              {showFixedQuoteNoInfo && <p>This is the minimum charge for this job and we will try our best to complete within this quoted amount. We may need more time, if so we’ll provide a quote for your approval before we proceed</p>}
            </div>
            <br />
            <p>Handyman Required:</p>
            <br />
            <div>
              <input type="radio" id="yes" name="handymanRequired" value="1" checked={product.handymanRequired == '1'} readOnly />
              <label htmlFor="yes">Yes</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleHandymanInfoClick(e, 'yes')}>i</button>
              {showHandymanYesInfo && <p>A handyman will be needed after our visit, we will give you the details of someone we recommend and they will quote for the repairs separately</p>}
            </div>
            <div>
              <input type="radio" id="no" name="handymanRequired" value="2" checked={product.handymanRequired == '2'} readOnly />
              <label htmlFor="no">No</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleHandymanInfoClick(e, 'no')}>i</button>
              {showHandymanNoInfo && <p>We do not envision a handyman will be needed after our visit, if anything does arise, we will give you the details of someone we recommend and they will quote for the repairs separately</p>}
            </div>
            <br />
            <p>Description:</p>
            <br />
            <div tabIndex={0} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', border: '2px solid transparent', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap', overflow: 'auto' }} >
              <p>{details[0]?.description}</p>
            </div>
            {/* Display Total */}
            <div style={{ marginTop:'10px' }}>
              <p>Labour Total:</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{product.price.toFixed(2)}</div>
            </div>
            <br />
              {/* Submit Button */}
              <div style={{ maxWidth: '350px', width: '50%', margin: "0 auto" }}>
                  <button onClick={handleButtonClick} style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
                      <img src="/orangeapprovepaybutton.png" alt="approve" />
                  </button>
              </div>
          </form>
        </div>
        <br />
        <div style={{ textAlign: 'center' }}>
          <strong>Terms -</strong>
          <p>All work is covered by a 1 year guarantee <a className="text-blue-500 underline hover:text-blue-700" href="https://theplumber.ltd/terms-conditions/">T&C’s</a></p>
          <br />
          {product.fixedQuote == "1" ? <p>You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster</p> : <p>This is the minimum charge for this job and we will try our best to complete within this quoted amount. We may need more time, if so we’ll provide a quote for your approval before we proceed</p>}
          <br />
          {product.handymanRequired == "1" ? <p>A handyman will be needed after our visit, we will give you the details of someone we recommend and they will quote for the repairs separately</p> : <p>We do not envision a handyman will be needed after our visit, if anything does arise, we will give you the details of someone we recommend and they will quote for the repairs separately</p>}
        </div>
      </div>
      <Footer />
    </div>

  );
};

export default CustomerQuotePage3;

export const getServerSideProps = async (
    context: GetServerSidePropsContext
) => {
  const session: ISession | null = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};