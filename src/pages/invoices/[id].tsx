import Header from 'components/Header';
import { GetServerSidePropsContext } from "next";
import { useRouter } from 'next/router';
import { getSession, useSession } from "next-auth/react";
import { IProduct, ISession } from '../../../typings';
import axios from 'axios';
import { Stripe, loadStripe } from '@stripe/stripe-js';
let stripePromise: Promise<Stripe | null>;
import React, { useEffect, useState } from 'react';
import { collection, doc, getDoc, getDocs, query, where } from 'firebase/firestore';
import db from '../../../firebase';

const InvoicePage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: session } = useSession();
  const [product, setProduct] = useState<IProduct | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const details = product!.details;

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        // Check if the product is in the cache
        const cachedProduct = localStorage.getItem(`product-${id}`);
        if (cachedProduct) {
          setProduct(JSON.parse(cachedProduct) as IProduct);
          setLoading(false);
          return;
        }
  
        const productsRef = collection(db, 'products');
        const q = query(productsRef, where("id", "==", id));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const productDoc = querySnapshot.docs[0];
          const productData = { ...productDoc.data(), id: productDoc.id } as IProduct;
          setProduct(productData);
  
          // Cache the product
          localStorage.setItem(`product-${id}`, JSON.stringify(productData));
        } else {
          throw new Error('Product not found');
        }
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };

    fetchProduct();
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!product) {
    return <div><Header /><div>Invoice not found</div></div>;
  }

  // Calculate total cost
  const calculateCosts = () => {
    let totalCost = details.reduce((total: any, detail: any) => {
      const cost = detail.type === 'labour' ? detail.laborCost : detail.materialCost;
      return total + Number(cost) * Number(detail.quantity);
    }, 0);
    
    return { totalCost };
  };

  return (
    <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto' }}>
      <Header /> {/* Include the header */}
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px', marginBottom: '10px' }}>
        <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Invoice</span></h1>
        <form style={{ width: '100%' }}>

          {/* Labour and Material Details Section */}
          {details.map((detail: any, index: any) => (
            <div key={index} style={{ marginBottom: "10px", border: '1px solid #209ee6', padding: '10px' }}>
              <h2 style={{ color:'#000000', paddingBottom:'10px', paddingTop: '10px', margin:'5px 0', fontSize:'25px', fontWeight:'bold' }}>{detail.type.charAt(0).toUpperCase() + detail.type.slice(1)}</h2>
              <label>Description:</label>
              <br />
              <input type="text" name="description" value={detail.description} readOnly placeholder={`${detail.type.charAt(0).toUpperCase() + detail.type.slice(1)} Description`} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
              <br />
              <label>Quantity:</label>
              <input type="number" name="quantity" value={detail.quantity} readOnly placeholder="Quantity" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
              <br />
              <label>Unit Price (£):</label>
              <input type="number" name="cost" value={detail.cost} readOnly placeholder="Unit Price (£)" style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3' }} />
            </div>
          ))}

          {/* Display Subtotal, VAT and Total */}
          <div style={{ marginTop:'10px' }}>
            <p>Total:</p>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCost.toFixed(2)}</div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default InvoicePage;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  // Get user logged in credentials
  const session: ISession | null = await getSession(context);
  console.log('Session before:', session);

  return {
    props: {
      session,
    },
  };
};