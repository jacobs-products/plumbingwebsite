import HeaderPlumber from 'components/HeaderPlumber';
import { GetServerSidePropsContext } from "next";
import { useRouter } from 'next/router';
import { getSession, useSession } from "next-auth/react";
import { IProduct, ISession } from '../../../typings';
import axios from 'axios';
import { Stripe, loadStripe } from '@stripe/stripe-js';
import FooterQuestions from 'components/FooterQuestions';
let stripePromise: Promise<Stripe | null>;
import React, { useEffect, useState } from 'react';
import { collection, getDocs, query, where } from 'firebase/firestore';
import db from '../../../firebase';

const CostsPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: session } = useSession();
  const [product, setProduct] = useState<IProduct | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [showFixedQuoteYesInfo, setShowFixedQuoteYesInfo] = useState(false);
  const [showFixedQuoteNoInfo, setShowFixedQuoteNoInfo] = useState(false);
  let totalAmount: any;

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        // Check if the product is in the cache
        const cachedProduct = localStorage.getItem(`product-${id}`);
        if (cachedProduct) {
          setProduct(JSON.parse(cachedProduct) as IProduct);
          setLoading(false);
          return;
        }
  
        const productsRef = collection(db, 'products');
        const q = query(productsRef, where("id", "==", id));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const productDoc = querySnapshot.docs[0];
          const productData = { ...productDoc.data(), id: productDoc.id } as IProduct;
          setProduct(productData);
  
          // Cache the product
          localStorage.setItem(`product-${id}`, JSON.stringify(productData));
        } else {
          throw new Error('Product not found');
        }
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred');
        }
      } finally {
        setLoading(false);
      }
    };    

    fetchProduct();
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!product) {
    return <div><HeaderPlumber /><div>Costs not found</div></div>;
  }

  const details = product!.details;

  const calculateCosts = () => {
    let totalCardCost = details.reduce((total: any, detail: any) => {
      if (detail.type.includes('Cash')) return total;
      return total + Number(detail.cost) * Number(detail.quantity);
    }, 0);
  
    let totalCashCost = details.reduce((total: any, detail: any) => {
      if (!detail.type.includes('Cash')) return total;
      return total + Number(detail.cost) * Number(detail.quantity);
    }, 0);

    totalAmount = totalCardCost;
  
    return { totalCardCost, totalCashCost };
  };

  const handleFixedQuoteInfoClick = (e: any, type: any) => {
    e.stopPropagation();
    if (type === 'yes' && product.fixedQuote == "1") {
      setShowFixedQuoteYesInfo(!showFixedQuoteYesInfo);
    } else if (type === 'no' && product.fixedQuote != "1") {
      setShowFixedQuoteNoInfo(!showFixedQuoteNoInfo);
    }
  };

  const handlePayButtonClick = async () => {
    if (!stripePromise) {
      stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY!);
    }
  
    const data = {
      items: [{
        price: totalAmount,
        title: "Costs",
        quantity: 1,
        id: product.id,
      }],
      email: session?.user?.email || '',
      quote_id: id,
      contactNumber: product.clientDetails.contactNumber,
      paymentMethod: 'stripe',
      distribution: product.distribution
    };
  
    try {
      const response = await axios.post('/.netlify/functions/create-checkout-session-jack', data);
  
      const stripe = await stripePromise;
      if (stripe) {
        await stripe.redirectToCheckout({ sessionId: response.data.id });
      } else {
        console.error('Stripe failed to load');
      }
    } catch (error) {
      console.error('Error creating checkout session:', error);
    }
  };

  return (
    <div>
      <div style={{ justifyContent: 'center', alignItems: 'center', fontFamily: 'Arial, sans-serif', width: '75%', margin: '0 auto', paddingBottom: '15em' }}>
        <HeaderPlumber /> {/* Include the HeaderPlumber */}
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '5px', width: '100%', border: '2px solid #209ee6', marginTop: '100px', marginBottom: '10px' }}>
          <h1 style={{ color: '#000000', textAlign: 'center', fontSize: '40px', fontWeight: 'bold' }}><span style={{ borderBottom: '4px solid #209ee6' }}>Costs</span></h1>
          <form style={{ width: '100%' }}>
            <br />
            <p>Engineer:</p>
            <div style={{ backgroundColor: '#D3D3D3', padding: '10px' }}>{product?.engineer || 'N/A'}</div>
            <br />
            <p>Fixed Quote:</p>
            <br />
            <div>
              <input type="radio" id="yes" name="fixedQuote" value="1" checked={product.fixedQuote == '1'} readOnly />
              <label htmlFor="yes">Yes</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleFixedQuoteInfoClick(e, 'yes')}>i</button>
              {showFixedQuoteYesInfo && <p>You will not be charged any more if the job takes longer to complete, you will also not be charged any less if the job is completed faster</p>}
            </div>
            <div>
              <input type="radio" id="no" name="fixedQuote" value="2" checked={product.fixedQuote == '2'} readOnly />
              <label htmlFor="no">No</label>
              <button type="button" style={{ 
                display: 'inline-block',
                backgroundColor: '#D3D3D3',
                borderRadius: '50%',
                paddingLeft: '0.5em',
                paddingRight: '0.5em',
                fontSize: '0.8em',
                fontWeight: 'bold',
                position: 'relative',
                top: '-0.2em',
                marginLeft: '5px'
              }} onClick={(e) => handleFixedQuoteInfoClick(e, 'no')}>i</button>
              {showFixedQuoteNoInfo && <p>This is the minimum charge for this job and we will try our best to complete within this quoted amount. We may need more time, if so we’ll provide a quote for your approval before we proceed</p>}
            </div>
            <br />
            <p>Description:</p>
            <br />
            <div tabIndex={0} style={{ marginBottom:'10px', backgroundColor:'#FFFFFF', padding:'10px', margin:'5px 0', width:'100%', borderRadius: '5px', border: '2px solid transparent', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap', overflow: 'auto' }} >
              <p>{details[0]?.description}</p>
            </div>
            <label htmlFor="unableToGuarantee">Unable to Guarantee:</label>
            <br />
            <textarea style={{ marginBottom: '10px', backgroundColor: '#FFFFFF', padding: '10px', margin: '5px 0', width: '100%', borderRadius: '5px', borderBottom: '2px solid #D3D3D3', whiteSpace: 'pre-wrap' }} id="unableToGuarantee" name="unableToGuarantee" value={product.unableToGuarantee} readOnly />
            <br />
            {/* Display Total */}
            <div style={{ marginTop:'10px' }}>
              <strong>Labour</strong>
              <br />
              <p>Cost (£):</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[0]?.cost}</div>
              <br />
              <strong>Materials</strong>
              <br />
              <p>Cost (£):</p>
              <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[1]?.cost}</div>
            </div>
            <br />
            <strong>Total</strong>
            <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCardCost.toFixed(2)}</div>
            <br />
            </form>
            <div style={{ maxWidth: '350px', width: '50%' }}>
              <button onClick={handlePayButtonClick} style={{ background: "none", border: "none", padding: 0, width: '100%' }}>
                <img src="/orangeapprovepaybutton.png" alt="Pay" />
              </button>
            </div>
            <br />
            {product.cashPrice === '1' && (
              <form style={{ width: '100%' }}>
                <p style={{ color: '#209ee6', textAlign: 'center', marginBottom: '10px' }}><strong>See cash price below</strong></p>
                <div style={{ borderTop: '4px solid #000000', marginTop: '20px' }}>
                  <div style={{ marginTop:'10px' }}>
                    <strong>Labour (Cash)</strong>
                    <br />
                    <p>Cost (£):</p>
                    <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[2]?.cost}</div>
                    <br />
                    <strong>Materials (Cash)</strong>
                    <br />
                    <p>Cost (£):</p>
                    <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{details[3]?.cost}</div>
                  </div>
                  <br />
                  <strong>Total (Cash)</strong>
                  <div style={{ backgroundColor:'#D3D3D3', padding:'10px' }}>£{calculateCosts().totalCashCost.toFixed(2)}</div>
                  <br />
                </div>
              </form>
            )}
            {product.cashPrice === '1' && (
            <div style={{ maxWidth: '350px', width: '50%', marginTop: '-10px' }}>
              <a 
              href="sms:07451275575?body=Hi, I'd like to approve the quote and pay via cash please." 
              style={{ background: "none", border: "none", padding: 0, width: '100%', display: 'block', textAlign: 'center' }}
              onClick={(e) => {
                if (!navigator.userAgent.match(/Android|iPhone|iPad|iPod/i)) {
                e.preventDefault();
                alert("Please send a text to the following number to approve your quote: 07451275575");
                }
              }}
              >
              <img src="/orangeapprovecashbutton.png" alt="Approve Cash" />
              </a>
            </div>
          )}
        </div>
        <br />
        <div style={{ textAlign: 'center' }}>
          <strong>All work is covered by a 1 year guarantee <a className="text-blue-500 underline hover:text-blue-700" href="https://theplumber.ltd/terms-conditions/">T&C’s</a> apply</strong>
        </div>
      </div>
      <FooterQuestions />
    </div>
  );
};

export default CostsPage;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: ISession | null = await getSession(context).catch((error) => {
    console.error('Error getting session:', error);
    return null;
  });

  return {
    props: {
      session,
    },
  };
};
