export interface IProduct {
  id: string;
  title: string;
  price: number;
  description?: string;
  category?: string;
  subcategory?: string;
  subsubcategory?: string;
  fixedQuote?: string;
  handymanRequired?: string;
  cashPrice?: string;
  reason?: string;
  guarantee?: string;
  image: string;
  quantity: number;
  paymentConfirmed?: string;
  clientDetails: {
    clientName: string;
    street: string;
    city: string;
    county: string;
    postcode: string;
    contactNumber: string;
  };
  labourDetails: Array<{
    description: string,
    quantity: string,
    laborCost: string
  }>;
  materialDetails: Array<{
    description: string,
    quantity: string,
    materialCost: string
  }>;
  details: Array<{ type: string, description: string, quantity: string, cost: string }>;
  distribution?: {
    labour?: { elliot?: number; jack?: number };
    material?: { elliot?: number; jack?: number };
  };
  engineer?: string;
  unableToGuarantee?: string;
  customer?: string;
  date?: string;
}

export interface IOrder {
  id: number;
  title: string;
  amount: number;
  amount_shipping: number;
  timestamp: number;
  images: string[];
  quote_id: string;
}

export interface ISession {
  user: {
    name: string;
    email: string;
    image: string;
    address: string;
  } & DefaultSession["user"];
  expires: string;
}
